﻿namespace monteCarloUct
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.startButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ucbPolicySimuNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slot1ConstXLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.slot1UcbLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.slot1SimuNumLabel = new System.Windows.Forms.Label();
            this.slot1AvgCoinNumLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.useSlot1CheckBox = new System.Windows.Forms.CheckBox();
            this.ucbPolicyPlayButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.constXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.useSlot2CheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.slot2ConstXLabel = new System.Windows.Forms.Label();
            this.slot2UcbLabel = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.slot2ConstXLabel__ = new System.Windows.Forms.Label();
            this.slot2SimuNumLabel = new System.Windows.Forms.Label();
            this.slot2AvgCoinNumLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.slot3ConstXLabel = new System.Windows.Forms.Label();
            this.slot3UcbLabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.slot3ConstXLabel__ = new System.Windows.Forms.Label();
            this.slot3SimuNumLabel = new System.Windows.Forms.Label();
            this.slot3AvgCoinNumLabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.n = new System.Windows.Forms.GroupBox();
            this.slot4ConstXLabel = new System.Windows.Forms.Label();
            this.slot4UcbLabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.slot4SimuNumLabel = new System.Windows.Forms.Label();
            this.slot4AvgCoinNumLabel = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.useSlot3CheckBox = new System.Windows.Forms.CheckBox();
            this.useSlot4CheckBox = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.totalSimuNumLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.n.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(4, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 34);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(0, 67);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(362, 64);
            this.startButton.TabIndex = 10;
            this.startButton.Text = "全てのスロットを1回試して、開始する(最初の1回だけ必要)";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartPlay);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(6, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "最大UCB値に従って";
            // 
            // ucbPolicySimuNumTextBox
            // 
            this.ucbPolicySimuNumTextBox.Enabled = false;
            this.ucbPolicySimuNumTextBox.Location = new System.Drawing.Point(185, 145);
            this.ucbPolicySimuNumTextBox.Mask = "00000";
            this.ucbPolicySimuNumTextBox.Name = "ucbPolicySimuNumTextBox";
            this.ucbPolicySimuNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.ucbPolicySimuNumTextBox.TabIndex = 12;
            this.ucbPolicySimuNumTextBox.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(304, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "回数";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(0, 185);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(198, 55);
            this.resetButton.TabIndex = 14;
            this.resetButton.Text = "リセットする";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.SelectedResetButton);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slot1ConstXLabel);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.slot1UcbLabel);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.slot1SimuNumLabel);
            this.groupBox1.Controls.Add(this.slot1AvgCoinNumLabel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(6, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(389, 247);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "スロット1";
            // 
            // slot1ConstXLabel
            // 
            this.slot1ConstXLabel.AutoSize = true;
            this.slot1ConstXLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot1ConstXLabel.Location = new System.Drawing.Point(89, 119);
            this.slot1ConstXLabel.Name = "slot1ConstXLabel";
            this.slot1ConstXLabel.Size = new System.Drawing.Size(69, 20);
            this.slot1ConstXLabel.TabIndex = 25;
            this.slot1ConstXLabel.Text = "未確定";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(0, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 20);
            this.label18.TabIndex = 25;
            this.label18.Text = "定数X  =";
            // 
            // slot1UcbLabel
            // 
            this.slot1UcbLabel.AutoSize = true;
            this.slot1UcbLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot1UcbLabel.Location = new System.Drawing.Point(89, 216);
            this.slot1UcbLabel.Name = "slot1UcbLabel";
            this.slot1UcbLabel.Size = new System.Drawing.Size(33, 20);
            this.slot1UcbLabel.TabIndex = 25;
            this.slot1UcbLabel.Text = "0.0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(0, 216);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 20);
            this.label17.TabIndex = 24;
            this.label17.Text = "UCB値 =";
            // 
            // slot1SimuNumLabel
            // 
            this.slot1SimuNumLabel.AutoSize = true;
            this.slot1SimuNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot1SimuNumLabel.Location = new System.Drawing.Point(192, 181);
            this.slot1SimuNumLabel.Name = "slot1SimuNumLabel";
            this.slot1SimuNumLabel.Size = new System.Drawing.Size(19, 20);
            this.slot1SimuNumLabel.TabIndex = 12;
            this.slot1SimuNumLabel.Text = "0";
            // 
            // slot1AvgCoinNumLabel
            // 
            this.slot1AvgCoinNumLabel.AutoSize = true;
            this.slot1AvgCoinNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot1AvgCoinNumLabel.Location = new System.Drawing.Point(178, 145);
            this.slot1AvgCoinNumLabel.Name = "slot1AvgCoinNumLabel";
            this.slot1AvgCoinNumLabel.Size = new System.Drawing.Size(33, 20);
            this.slot1AvgCoinNumLabel.TabIndex = 11;
            this.slot1AvgCoinNumLabel.Text = "0.0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(159, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "期待値 = 5.5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(108, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(223, 40);
            this.label7.TabIndex = 9;
            this.label7.Text = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]\r\n枚のコインが均等に出てくる";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(-4, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "各スロット試行回数  =";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "コイン平均獲得数  =";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(27, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(732, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "UCB値 = コイン平均獲得数 + (定数X * Math.Sqrt(2 * Math.Log(合計スロット試行回数) / 各スロット試行回数))";
            // 
            // useSlot1CheckBox
            // 
            this.useSlot1CheckBox.AutoSize = true;
            this.useSlot1CheckBox.Checked = true;
            this.useSlot1CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useSlot1CheckBox.Location = new System.Drawing.Point(6, 68);
            this.useSlot1CheckBox.Name = "useSlot1CheckBox";
            this.useSlot1CheckBox.Size = new System.Drawing.Size(115, 16);
            this.useSlot1CheckBox.TabIndex = 18;
            this.useSlot1CheckBox.Text = "スロット1を使用する";
            this.useSlot1CheckBox.UseVisualStyleBackColor = true;
            // 
            // ucbPolicyPlayButton
            // 
            this.ucbPolicyPlayButton.Enabled = false;
            this.ucbPolicyPlayButton.Location = new System.Drawing.Point(365, 137);
            this.ucbPolicyPlayButton.Name = "ucbPolicyPlayButton";
            this.ucbPolicyPlayButton.Size = new System.Drawing.Size(128, 34);
            this.ucbPolicyPlayButton.TabIndex = 19;
            this.ucbPolicyPlayButton.Text = "スロットを試行する";
            this.ucbPolicyPlayButton.UseVisualStyleBackColor = true;
            this.ucbPolicyPlayButton.Click += new System.EventHandler(this.UcbPolicyPlayEvent);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.constXTextBox);
            this.groupBox2.Controls.Add(this.startButton);
            this.groupBox2.Controls.Add(this.ucbPolicyPlayButton);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.ucbPolicySimuNumTextBox);
            this.groupBox2.Controls.Add(this.resetButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(42, 376);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(748, 246);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(4, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "定数X  =";
            // 
            // constXTextBox
            // 
            this.constXTextBox.Location = new System.Drawing.Point(93, 34);
            this.constXTextBox.Mask = "000";
            this.constXTextBox.Name = "constXTextBox";
            this.constXTextBox.Size = new System.Drawing.Size(100, 19);
            this.constXTextBox.TabIndex = 20;
            this.constXTextBox.Text = "10";
            // 
            // useSlot2CheckBox
            // 
            this.useSlot2CheckBox.AutoSize = true;
            this.useSlot2CheckBox.Checked = true;
            this.useSlot2CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useSlot2CheckBox.Location = new System.Drawing.Point(401, 68);
            this.useSlot2CheckBox.Name = "useSlot2CheckBox";
            this.useSlot2CheckBox.Size = new System.Drawing.Size(115, 16);
            this.useSlot2CheckBox.TabIndex = 21;
            this.useSlot2CheckBox.Text = "スロット2を使用する";
            this.useSlot2CheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.slot2ConstXLabel);
            this.groupBox3.Controls.Add(this.slot2UcbLabel);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.slot2ConstXLabel__);
            this.groupBox3.Controls.Add(this.slot2SimuNumLabel);
            this.groupBox3.Controls.Add(this.slot2AvgCoinNumLabel);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.pictureBox2);
            this.groupBox3.Location = new System.Drawing.Point(401, 90);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(389, 247);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "スロット2";
            // 
            // slot2ConstXLabel
            // 
            this.slot2ConstXLabel.AutoSize = true;
            this.slot2ConstXLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot2ConstXLabel.Location = new System.Drawing.Point(85, 119);
            this.slot2ConstXLabel.Name = "slot2ConstXLabel";
            this.slot2ConstXLabel.Size = new System.Drawing.Size(69, 20);
            this.slot2ConstXLabel.TabIndex = 30;
            this.slot2ConstXLabel.Text = "未確定";
            // 
            // slot2UcbLabel
            // 
            this.slot2UcbLabel.AutoSize = true;
            this.slot2UcbLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot2UcbLabel.Location = new System.Drawing.Point(92, 216);
            this.slot2UcbLabel.Name = "slot2UcbLabel";
            this.slot2UcbLabel.Size = new System.Drawing.Size(33, 20);
            this.slot2UcbLabel.TabIndex = 29;
            this.slot2UcbLabel.Text = "0.0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(2, 216);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 20);
            this.label20.TabIndex = 28;
            this.label20.Text = "UCB値 =";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(2, 181);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(180, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "各スロット試行回数 =";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // slot2ConstXLabel__
            // 
            this.slot2ConstXLabel__.AutoSize = true;
            this.slot2ConstXLabel__.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot2ConstXLabel__.Location = new System.Drawing.Point(2, 119);
            this.slot2ConstXLabel__.Name = "slot2ConstXLabel__";
            this.slot2ConstXLabel__.Size = new System.Drawing.Size(83, 20);
            this.slot2ConstXLabel__.TabIndex = 26;
            this.slot2ConstXLabel__.Text = "定数X  =";
            // 
            // slot2SimuNumLabel
            // 
            this.slot2SimuNumLabel.AutoSize = true;
            this.slot2SimuNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot2SimuNumLabel.Location = new System.Drawing.Point(188, 181);
            this.slot2SimuNumLabel.Name = "slot2SimuNumLabel";
            this.slot2SimuNumLabel.Size = new System.Drawing.Size(19, 20);
            this.slot2SimuNumLabel.TabIndex = 13;
            this.slot2SimuNumLabel.Text = "0";
            // 
            // slot2AvgCoinNumLabel
            // 
            this.slot2AvgCoinNumLabel.AutoSize = true;
            this.slot2AvgCoinNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot2AvgCoinNumLabel.Location = new System.Drawing.Point(174, 145);
            this.slot2AvgCoinNumLabel.Name = "slot2AvgCoinNumLabel";
            this.slot2AvgCoinNumLabel.Size = new System.Drawing.Size(33, 20);
            this.slot2AvgCoinNumLabel.TabIndex = 12;
            this.slot2AvgCoinNumLabel.Text = "0.0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(2, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 20);
            this.label12.TabIndex = 4;
            this.label12.Text = "コイン平均獲得数 =";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(52, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 20);
            this.label10.TabIndex = 3;
            this.label10.Text = "期待値 = 5";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(52, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(192, 40);
            this.label9.TabIndex = 2;
            this.label9.Text = "[1, 3, 5, 7, 9]枚のコイン\r\nが均等に出てくる";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 18);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 34);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.slot3ConstXLabel);
            this.groupBox4.Controls.Add(this.slot3UcbLabel);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.slot3ConstXLabel__);
            this.groupBox4.Controls.Add(this.slot3SimuNumLabel);
            this.groupBox4.Controls.Add(this.slot3AvgCoinNumLabel);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.pictureBox3);
            this.groupBox4.Location = new System.Drawing.Point(796, 90);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 247);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "スロット3";
            // 
            // slot3ConstXLabel
            // 
            this.slot3ConstXLabel.AutoSize = true;
            this.slot3ConstXLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot3ConstXLabel.Location = new System.Drawing.Point(82, 119);
            this.slot3ConstXLabel.Name = "slot3ConstXLabel";
            this.slot3ConstXLabel.Size = new System.Drawing.Size(69, 20);
            this.slot3ConstXLabel.TabIndex = 30;
            this.slot3ConstXLabel.Text = "未確定";
            // 
            // slot3UcbLabel
            // 
            this.slot3UcbLabel.AutoSize = true;
            this.slot3UcbLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot3UcbLabel.Location = new System.Drawing.Point(92, 216);
            this.slot3UcbLabel.Name = "slot3UcbLabel";
            this.slot3UcbLabel.Size = new System.Drawing.Size(33, 20);
            this.slot3UcbLabel.TabIndex = 29;
            this.slot3UcbLabel.Text = "0.0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(2, 216);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 20);
            this.label21.TabIndex = 28;
            this.label21.Text = "UCB値 =";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.Location = new System.Drawing.Point(2, 181);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(180, 20);
            this.label22.TabIndex = 27;
            this.label22.Text = "各スロット試行回数 =";
            // 
            // slot3ConstXLabel__
            // 
            this.slot3ConstXLabel__.AutoSize = true;
            this.slot3ConstXLabel__.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot3ConstXLabel__.Location = new System.Drawing.Point(2, 119);
            this.slot3ConstXLabel__.Name = "slot3ConstXLabel__";
            this.slot3ConstXLabel__.Size = new System.Drawing.Size(83, 20);
            this.slot3ConstXLabel__.TabIndex = 26;
            this.slot3ConstXLabel__.Text = "定数X  =";
            // 
            // slot3SimuNumLabel
            // 
            this.slot3SimuNumLabel.AutoSize = true;
            this.slot3SimuNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot3SimuNumLabel.Location = new System.Drawing.Point(188, 181);
            this.slot3SimuNumLabel.Name = "slot3SimuNumLabel";
            this.slot3SimuNumLabel.Size = new System.Drawing.Size(19, 20);
            this.slot3SimuNumLabel.TabIndex = 13;
            this.slot3SimuNumLabel.Text = "0";
            // 
            // slot3AvgCoinNumLabel
            // 
            this.slot3AvgCoinNumLabel.AutoSize = true;
            this.slot3AvgCoinNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot3AvgCoinNumLabel.Location = new System.Drawing.Point(176, 145);
            this.slot3AvgCoinNumLabel.Name = "slot3AvgCoinNumLabel";
            this.slot3AvgCoinNumLabel.Size = new System.Drawing.Size(33, 20);
            this.slot3AvgCoinNumLabel.TabIndex = 12;
            this.slot3AvgCoinNumLabel.Text = "0.0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.Location = new System.Drawing.Point(2, 145);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(168, 20);
            this.label26.TabIndex = 4;
            this.label26.Text = "コイン平均獲得数 =";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label27.Location = new System.Drawing.Point(50, 82);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 20);
            this.label27.TabIndex = 3;
            this.label27.Text = "期待値 = 6";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(52, 18);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(202, 40);
            this.label28.TabIndex = 2;
            this.label28.Text = "[2, 4, 6, 8, 10]枚のコイン\r\nが均等に出てくる";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(6, 18);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 34);
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // n
            // 
            this.n.Controls.Add(this.slot4ConstXLabel);
            this.n.Controls.Add(this.slot4UcbLabel);
            this.n.Controls.Add(this.label19);
            this.n.Controls.Add(this.label23);
            this.n.Controls.Add(this.label24);
            this.n.Controls.Add(this.slot4SimuNumLabel);
            this.n.Controls.Add(this.slot4AvgCoinNumLabel);
            this.n.Controls.Add(this.label30);
            this.n.Controls.Add(this.label31);
            this.n.Controls.Add(this.label32);
            this.n.Controls.Add(this.pictureBox4);
            this.n.Location = new System.Drawing.Point(796, 375);
            this.n.Name = "n";
            this.n.Size = new System.Drawing.Size(389, 247);
            this.n.TabIndex = 26;
            this.n.TabStop = false;
            this.n.Text = "スロット4";
            // 
            // slot4ConstXLabel
            // 
            this.slot4ConstXLabel.AutoSize = true;
            this.slot4ConstXLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot4ConstXLabel.Location = new System.Drawing.Point(85, 119);
            this.slot4ConstXLabel.Name = "slot4ConstXLabel";
            this.slot4ConstXLabel.Size = new System.Drawing.Size(69, 20);
            this.slot4ConstXLabel.TabIndex = 30;
            this.slot4ConstXLabel.Text = "未確定";
            // 
            // slot4UcbLabel
            // 
            this.slot4UcbLabel.AutoSize = true;
            this.slot4UcbLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot4UcbLabel.Location = new System.Drawing.Point(85, 216);
            this.slot4UcbLabel.Name = "slot4UcbLabel";
            this.slot4UcbLabel.Size = new System.Drawing.Size(33, 20);
            this.slot4UcbLabel.TabIndex = 29;
            this.slot4UcbLabel.Text = "0.0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(1, 216);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 20);
            this.label19.TabIndex = 28;
            this.label19.Text = "UCB値 =";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label23.Location = new System.Drawing.Point(2, 181);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(180, 20);
            this.label23.TabIndex = 27;
            this.label23.Text = "各スロット試行回数 =";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label24.Location = new System.Drawing.Point(2, 119);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 20);
            this.label24.TabIndex = 26;
            this.label24.Text = "定数X  =";
            // 
            // slot4SimuNumLabel
            // 
            this.slot4SimuNumLabel.AutoSize = true;
            this.slot4SimuNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot4SimuNumLabel.Location = new System.Drawing.Point(188, 181);
            this.slot4SimuNumLabel.Name = "slot4SimuNumLabel";
            this.slot4SimuNumLabel.Size = new System.Drawing.Size(19, 20);
            this.slot4SimuNumLabel.TabIndex = 13;
            this.slot4SimuNumLabel.Text = "0";
            // 
            // slot4AvgCoinNumLabel
            // 
            this.slot4AvgCoinNumLabel.AutoSize = true;
            this.slot4AvgCoinNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.slot4AvgCoinNumLabel.Location = new System.Drawing.Point(176, 145);
            this.slot4AvgCoinNumLabel.Name = "slot4AvgCoinNumLabel";
            this.slot4AvgCoinNumLabel.Size = new System.Drawing.Size(33, 20);
            this.slot4AvgCoinNumLabel.TabIndex = 12;
            this.slot4AvgCoinNumLabel.Text = "0.0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.Location = new System.Drawing.Point(2, 145);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(168, 20);
            this.label30.TabIndex = 4;
            this.label30.Text = "コイン平均獲得数 =";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(52, 82);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 20);
            this.label31.TabIndex = 3;
            this.label31.Text = "期待値 = 10";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label32.Location = new System.Drawing.Point(52, 18);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(194, 40);
            this.label32.TabIndex = 2;
            this.label32.Text = "99%の確率でコイン0枚\r\n1%の確率でコイン100枚";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(6, 18);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 34);
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // useSlot3CheckBox
            // 
            this.useSlot3CheckBox.AutoSize = true;
            this.useSlot3CheckBox.Checked = true;
            this.useSlot3CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useSlot3CheckBox.Location = new System.Drawing.Point(796, 68);
            this.useSlot3CheckBox.Name = "useSlot3CheckBox";
            this.useSlot3CheckBox.Size = new System.Drawing.Size(115, 16);
            this.useSlot3CheckBox.TabIndex = 27;
            this.useSlot3CheckBox.Text = "スロット3を使用する";
            this.useSlot3CheckBox.UseVisualStyleBackColor = true;
            // 
            // useSlot4CheckBox
            // 
            this.useSlot4CheckBox.AutoSize = true;
            this.useSlot4CheckBox.Location = new System.Drawing.Point(796, 353);
            this.useSlot4CheckBox.Name = "useSlot4CheckBox";
            this.useSlot4CheckBox.Size = new System.Drawing.Size(115, 16);
            this.useSlot4CheckBox.TabIndex = 28;
            this.useSlot4CheckBox.Text = "スロット4を使用する";
            this.useSlot4CheckBox.UseVisualStyleBackColor = true;
            this.useSlot4CheckBox.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(198, 353);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(206, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "合計スロット試行回数  =";
            // 
            // totalSimuNumLabel
            // 
            this.totalSimuNumLabel.AutoSize = true;
            this.totalSimuNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.totalSimuNumLabel.Location = new System.Drawing.Point(410, 353);
            this.totalSimuNumLabel.Name = "totalSimuNumLabel";
            this.totalSimuNumLabel.Size = new System.Drawing.Size(19, 20);
            this.totalSimuNumLabel.TabIndex = 30;
            this.totalSimuNumLabel.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 647);
            this.Controls.Add(this.totalSimuNumLabel);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.useSlot4CheckBox);
            this.Controls.Add(this.useSlot3CheckBox);
            this.Controls.Add(this.n);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.useSlot2CheckBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.useSlot1CheckBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.n.ResumeLayout(false);
            this.n.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox ucbPolicySimuNumTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox useSlot1CheckBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button ucbPolicyPlayButton;
        private System.Windows.Forms.Label slot1SimuNumLabel;
        private System.Windows.Forms.Label slot1AvgCoinNumLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox constXTextBox;
        private System.Windows.Forms.CheckBox useSlot2CheckBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label slot2SimuNumLabel;
        private System.Windows.Forms.Label slot2AvgCoinNumLabel;
        private System.Windows.Forms.Label slot1UcbLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label slot1ConstXLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label slot2UcbLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label slot2ConstXLabel__;
        private System.Windows.Forms.Label slot2ConstXLabel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label slot3ConstXLabel;
        private System.Windows.Forms.Label slot3UcbLabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label slot3ConstXLabel__;
        private System.Windows.Forms.Label slot3SimuNumLabel;
        private System.Windows.Forms.Label slot3AvgCoinNumLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox n;
        private System.Windows.Forms.Label slot4ConstXLabel;
        private System.Windows.Forms.Label slot4UcbLabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label slot4SimuNumLabel;
        private System.Windows.Forms.Label slot4AvgCoinNumLabel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.CheckBox useSlot3CheckBox;
        private System.Windows.Forms.CheckBox useSlot4CheckBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label totalSimuNumLabel;
    }
}

